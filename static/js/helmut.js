$(document).ready(function () {
    "use strict";

    $("#start").on("click", start);

    function start() {
        $("#start").off();
        $("#start").prop("class", "btn btn-danger");
        $("#start").html("STOP");
        $("#deg2700").prop("class", "bulb on");
        let interval = setInterval(changeBulb, 1000);
        $("#start").on("click", function() { stop(interval); });
    }
    
    function stop(interval) {
        $("#start").off();
        clearInterval(interval);
        $(".on").attr("class", "bulb off");
        $("#start").prop("class", "btn btn-success");
        $("#start").html("START");
        $("#start").on("click", start);
    }

    function changeBulb() {
        let current_on = $(".on").attr("id");
        let plusOrMinus = Math.random() < 0.5 ? -1 : 1; // Math.round(Math.random()) * 2 - 1;
        let next_on = nextId(current_on, plusOrMinus);
        $("#" + current_on).attr("class", "bulb off");
        $("#" + next_on).attr("class", "bulb on");
        //console.log(current_on, plusOrMinus, next_on)
    }

    function nextId(current_id, direction) {
        current_id = Number.parseInt(current_id.substring(3));
        if (current_id==0 && direction<0) current_id = 3600;
        let next_id = current_id + 225*direction;
        if (next_id==3600 && direction>0) next_id = 0;
        return "deg" + pad(next_id, 4);
    }

    function pad(num, size) {
        let s = num+"";
        while (s.length < size) s = "0" + s;
        return s;
    }

});